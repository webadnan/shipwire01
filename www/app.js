/*
 * SM Adnan, 23 MAR 2014
 */
Ext.Loader.setConfig({
    paths: {
        Ext: '.',
        shipwire: 'app',
        'Ext.device': './device/',
        Sqlite: './sqlite'
    }
});

Ext.application({

    requires: [
		'shipwire.util.CallbackChain',
		'shipwire.util.GenericMap',
		'shipwire.util.Global',
		'shipwire.util.IgnoreTapWithin',
		'shipwire.util.InitSQLite',
		'shipwire.util.User',
		'shipwire.util.View',
		'Ext.device.Connection',
		'Ext.device.Notification',
    ],
    min: 0,
    sec: 0,
    timerContinue: false,
    ringdrops: 0,
    currDrillSel: 0,
    sutureDrops: 0,
    needleDrops: 0,
    currConsoleSurgeonID: 0,
    currConsoleSurgeonName: '',
    showGrades: false,
    timerStart: '0',
    models: [
        'Products'
    ],
    stores: [
    ],
    views: [
        'LoginContainer',
        'MainContainer',
        'Home',
        'ProductCatalog',
        'AddProduct',
        'UserInfo',
    ],
    controllers: [
        'Main'
    ],
    name: 'shipwire',
    statusBarStyle: 'black-translucent',
    startupImage: 'appresource/loading/Default.png',

    launch: function() {
        Ext.create('shipwire.view.MainContainer', {fullscreen: true});
    },

    setDB: function() {

    }
});
