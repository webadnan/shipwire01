/**
 * SM Adnan, added on Jan 31 2014
 */
Ext.define('shipwire.util.CallbackChain', {
	singleton: true,
	requires: [
	],
	callbacks: [],
	titles: [],
	index: 0,
	addCallback: function(callback, title){
		this.callbacks.push(callback);
		if (typeof(title) == 'undefined'){
			title = "Callback " + this.callbacks.length;
		}
		this.titles.push(title);
		return CallbackChain();
	},
	clear: function(){
		this.index = 0;
		this.callbacks = [];
		this.titles = [];
	},
	start: function(){
		var me = CallbackChain();
		if (me.index == me.callbacks.length){
			Ext.Viewport.setMasked(false);
			return;
		}
		me.callbacks[me.index++](me.start);
	}
});
var CallbackChain = function(){return shipwire.util.CallbackChain;};
