/**
 * SM Adnan, added on Feb 25 2014
 */
Ext.define('shipwire.util.View', {
	singleton : true,

	/**
	 * Queue of pages
	 */
	pages : [],

	locked : false,

	listOfPages : [  ],
			
	version: 0,

	currentView : function() {
		if (View().pages.length == 0) {
			return null;
		}
		return View().pages[View().pages.length - 1];
	},

	previousView : function() {
		var me = View();
		if (View().pages.length < 2) {
			return null;
		}
		return View().pages[View().pages.length - 2];
	},

	showPrevious : function() {
    	if (IgnoreTapWithin().check("View.showPrevious", 500)){
    		return;
    	}
		var me = View();
		
		var preView = me.previousView();
		if (preView == null) {
			var index = me.listOfPages.indexOf(me.currentView());
			if (index > 0) {
				me.pages.push(me.listOfPages[index - 1]);
				me.pages.reverse();
				return me.showPrevious();
			} else {
				return;
			}
		}
		me.show(preView);

		if (me.pages.length >= 3) {
			me.pages.pop();
			me.pages.pop();
		}
	},

	show : function(viewName, params, callbackCnt, version) {
		var me = View();
		Global().log("********** " + new Date().getTime());
		if (typeof(callbackCnt) == 'undefined'){
			callbackCnt = 0;
		}
		if (typeof(version) == 'undefined'){
			version = new Date().getTime();
		}
		if (version < me.version){
			return;
		}
		if (callbackCnt >= 5){
			me.locked = false;
		}
		var preView = me.currentView();
		if (viewName === preView){
			return;
		}
		
		if (preView == "AddProduct"){
			Global().currentProduct = null;
		}
		
		if (me.locked || typeof (Ext.getCmp('MainContainer')) == 'undefined') {
			Global().log("** (loop) Current View: "+viewName+", Previous View: "+preView+", lock");
			setTimeout(me.show, 1000, viewName, params, callbackCnt, version);
			return;
		}
		me.version = version;
		Global().log("** Current View: "+viewName+", Previous View: "+preView+", lock");
		me.locked = true;

		Global().log('show("' + viewName + '")');

		if (typeof (params) == 'undefined') {
			params = {};
		}
		if (typeof (params.direction) == 'undefined') {
			params.direction = 'right';
		}
		if (typeof (params.reverse) == 'undefined') {
			params.reverse = true;
		}

		// Loading the view and sliding
		if (typeof (Ext.getCmp(viewName)) == 'undefined') {
			Ext.getCmp('MainContainer').add({
				xtype : viewName,
				id : viewName
			});
		}
		Ext.getCmp('MainContainer').animateActiveItem(Ext.getCmp(viewName), {
			type : 'slide',
			direction : params.direction
		});

		if (preView != null) {
			Global().log("** Current View: "+viewName+", Previous View: "+preView+", setTimeout 1");
			setTimeout(function() {
				Global().log("** Current View: "+viewName+", Previous View: "+preView+", unlock 1");
				me.locked = false;
				Ext.getCmp(preView).destroy(null, null);
				Global().log(preView + ' destroyed');
			}, 500);
		}  else {
			Global().log("** Current View: "+viewName+", Previous View: "+preView+", unlock 3");
			me.locked = false;
		}

		me.pages.push(viewName);

		// Special case, for top page (consoleSurgeonSel) it will clear cache
		if (viewName == me.listOfPages[0]) {
			me.pages = [ viewName ];
		}
	}
});

var View = function() {
	return shipwire.util.View;
};
