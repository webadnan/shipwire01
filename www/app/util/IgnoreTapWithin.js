/**
 * SM Adnan, added on Mar 17 2014
 * 
 * If a button is pressed twice or more in same time then it will allow the first click ignoring the rest
 */
Ext.define('shipwire.util.IgnoreTapWithin', {
	singleton: true,
	requires: [
	],
	keys: {},
	
	/**
	 * Returns true if two tap/press is within duration. 
	 */
	check: function(key, duration){
		var me = IgnoreTapWithin();
		if (typeof(me.keys[key]) === 'undefined'){
			me.keys[key] = new Date().getTime();
			return false;
		}
		var diff = Math.abs(new Date().getTime() - me.keys[key]);
		if (diff < duration){
			//me.keys[key] = new Date().getTime();
			return true;
		}
		
		me.keys[key] = new Date().getTime();
		return false;
	}
});
var IgnoreTapWithin = function(){return shipwire.util.IgnoreTapWithin;};
