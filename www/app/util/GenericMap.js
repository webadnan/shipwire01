/**
 * SM Adnan, added on Jan 31 2014
 * The following constants will be loaded from database.
 * If those are not available in database then the default value associated with it will be loaded
 * Format: CONSTANT_NAME_IN_UPPERCASE: ['type', defaultValue]
 * 		type = ['int', 'float', 'string', 'url']
 * 		defaultValue will be according to the type
 *	Usages: GenericMap().CONSOLE_SURGEONS_DELTA_FREQ
 */
Ext.define('shipwire.util.GenericMap', {
	_list_of_constants: {
		PROJECT_TITLE:						['string', 'Shipwire Project'],
		
		// debug
		LIMIT_TOTAL_CONSOLE_SURGEONS:		['int', 99999],
		LIMIT_MAX_CONSOLE_SURGEONS_IN_VIEW:	['int', 50],
		
		// URLs
		SERVER_URL: 						['string', 'https://mwebdev.intusurg.com/TrainTrackDev/rest'],
		//SERVER_URL:     				    ['string', 'https://mwebdev.intusurg.com/TrainTrack/rest'],
		AUTHORIZATION_URL:					['url', '/authentication'],
		SKILL_SUBMISSIONS_URL:				['url', '/SubmitSkillAssessment'],
		SKILL_STATUS_FOR_CONTACT_URL:		['url', '/SubmitSkillAssessment/status/'],
		SKILL_SET_MASTER_DATA_URL:			['url', '/getMasterData'],
		CONSOLE_SURGEON_DELTA_URL:			['url', '/getConsoleSurgeons/'],
		
		// Auth
		AUTHORIZATION_KEY:					['string', 'Authorization'],
		AUTHORIZATION_VALUE:				['string', 'TRAINTRACK {USERNAME}:{PASSWORD}'],
		
		// Search criteria
		MIN_SEARCH_CHAR_COUNT:				['int', 2],
		
		// UI
		SHOW_ZZZ_WITH_NAME:					['int', 0],
		
		// Ignore some alert messages
		SHOW_NOTIFICATION_BACK_BUTTON:		['int', 1],
		SHOW_NOTIFICATION_LOGOUT_BUTTON:	['int', 0],
		
		// Notification text
		TEXT_SUBMIT_RESULTS:				['string', 'Submit Results for {0}?'],
		TEXT_SUCCESSFULLY_SUBMITTED:		['string', 'Successfully Submitted!'],
		TEXT_NO_MATCH_FOUND:				['string', 'No matches found'],
		TEXT_AT_LEAST_TWO_CHAT:				['string', 'The search text(s) should contain zero or at least two characters.'],
		TEXT_CHANGE_TO_INTAKE:				['string', "Do you want to change the type to INTAKE?"],
		TEXT_CHANGE_TO_EXIT:				['string', "Do you want to change the type to EXIT?"],
		TEXT_GO_BACK:						['string', "Are you sure you want to go back?"],
		
		// Initial skill tables
		SKILLS_TABLE:						['string', 'SKILLS_TABLE'],
		SKILL_TABLE_SI_MP:					['string', 'SI_MP_EXERCISES'],
		SKILL_TABLE_XI_MP:					['string', 'XI_MP_EXERCISES'],
		SKILL_TABLE_SI_SS:					['string', 'SI_SS_EXERCISES'],
		
		// Initial table text
		SKILL_TITLE_SI_MP:					['string', 'xxxx da Vinci Si Multiport Training'],
		SKILL_TITLE_XI_MP:					['string', 'xxxx da Vinci Xi Multiport Training'],
		SKILL_TITLE_SI_SS:					['string', 'xxxx da Vinci Si Single-Site Training'],
		
		// Skill status list, text and icon
		//SKILL_STATUSES:					['string', 'NOT_STARTED|IN_PROGRESS|COMPLETED'],
		NOT_STARTED:						['string', 'NOT_STARTED'],
		IN_PROGRESS:						['string', 'IN_PROGRESS'],
		INTAKE_IN_PROGRESS:					['string', 'INTAKE_IN_PROGRESS'],
		EXIT_IN_PROGRESS:					['string', 'EXIT_IN_PROGRESS'],
		COMPLETED:							['string', 'COMPLETED'],
		TEXT_NOT_STARTED:					['string', 'Not started'],
		TEXT_IN_PROGRESS:					['string', '<div style="margin-top:-9px">Intake complete<br>exit in progress</div>'],
		TEXT_INTAKE_IN_PROGRESS:			['string', 'Intake in progress'],
		TEXT_EXIT_IN_PROGRESS:				['string', '<div style="margin-top:-9px">Intake complete<br>exit in progress</div>'],
		TEXT_COMPLETED:						['string', 'Completed'],
		ICON_NOT_STARTED:					['string', '<img style="margin-left:3px;margin-top:3.5px;" src="appresource/icons/empty_gray_22_22.png">'],
		ICON_IN_PROGRESS:					['string', '<img src="appresource/icons/progress_blue_32_32.png">'],
		ICON_INTAKE_IN_PROGRESS:			['string', '<img src="appresource/icons/progress_blue_32_32.png">'],
		ICON_EXIT_IN_PROGRESS:				['string', '<img src="appresource/icons/progress_blue_32_32.png">'],
		ICON_COMPLETED:						['string', '<img style="margin-top:3px;" src="appresource/icons/check_green_22_22.png">'],
		
		// Timer
		TIMER_TEXT_START:					['string', 'Start'],
		TIMER_TEXT_STOP:					['string', 'Stop'],
		TIMER_TEXT_CONTINUE:				['string', 'Continue'],
		TIMER_TEXT_RESET:					['string', 'Reset'],
		
		// Submission
		SKILL_SUBMISSIONS_HEADERKEY:		['string', 'submissions'],
		
		// Timer related constants
		IDLE_TIMER_FREQ:					['int', 5000],
		CONSOLE_SURGEONS_DELTA_FREQ:		['int',	86400000],		// 1 day
		DATA_SYNC_FREQ: 					['int',	3600000],	   	// 1 hour
		//CONSOLE_SURGEONS_DELTA_FREQ:		['int',	1000*10],		// 1 day
		//DATA_SYNC_FREQ: 	   				['int',	1000*10],	   	// 1 hour
		PENDING_SUBMISSION_FREQ:			['int', 1000*10]		// 10 sec
	},
	singleton: true,
	requires: [
	],
	map: {}, // contains database values
	load: function(callback){
		Main().executeSql("select * from TT_GENERIC_MAP", function(tx, results){
			GenericMap().map = {}; // clear existing map
			for (var i=0;i<results.rows.length;i++){
				GenericMap().map[results.rows.item(i).name] = results.rows.item(i).value;
			}
			GenericMap()._setValues();
			Global().log('GenericMap loaded');
			Global().log("Environment: "+Global().env());
			callback();
		}, function(){
			GenericMap()._setValues();
			Global().log("Environment: "+Global().env());
			callback();
			Global().log('GenericMap loaded');
		});
	},
	_setValues: function(){
		var consts = this._list_of_constants;
		for (var key in consts){
			var type = consts[key][0];
			var defaultValue = consts[key][1];
			this[key] = this._get(key, type, defaultValue);
		}
	},
	/**
	 * Type can be ['string', 'int', 'float', 'url']
	 */
	_get: function(key, type, defaultValue){
		if (typeof(type) == 'undefined'){
			type = 'string';
		}
		if (typeof(defaultValue) == 'undefined'){
			defaultValue = null;
		}
		
		//var key = arguments.callee.caller.toString().match(/function ([^\(]+)/)[1];
		if (typeof(GenericMap().map[key]) == 'undefined'){
			//Global().log("GenericMap: The key "+key+" is missing in the database.");
			return (type == 'url') ? URLHelper(defaultValue) : defaultValue;
		}
		
		var value = GenericMap().map[key];
		if (value == null){
			//Global().log("GenericMap: The key "+key+" is missing in the database.");
			return (type == 'url') ? URLHelper(defaultValue) : defaultValue;
		} else if (type == 'string'){
			return value.toString();
		} else if (type == 'int'){
			return isNaN(value) ? defaultValue : parseInt(value);
		} else if (type == 'float'){
			return isNaN(value) ? defaultValue : parseFloat(value);
		} else if (type == 'url'){
			return URLHelper(value.toString());
		}
	},
	createSql: function(){
		var consts = this._list_of_constants;
		var sql = "delete from GENERIC_MAP;\n";
		for (var key in consts){
			var defaultValue = consts[key][1];
			sql += "insert into GENERIC_MAP (name,value,defaultValue) values ('"+key+"','"+this[key]+"','"+defaultValue+"');\n";
		}
		Global().log(sql);
	}
});
var GenericMap = function(){return shipwire.util.GenericMap;};
