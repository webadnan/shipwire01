/**
 * SM Adnan, added on Jan 16 2014
 */
Ext.define('shipwire.util.Global', {
	singleton: true,
	requires: [
		'Sqlite.Connection',
		'Sqlite.data.proxy.SqliteStorage',
		'Ext.data.reader.Array'
	],
	
	appResumeEventSet: false,
	appResume: false,
	
	// this object will contain the product to be edited
	selectedProduct: null,
	
	getScreenWidth: function(){
		//if (Ext.Viewport.getOrientation() === "landscape")
		return (window.innerWidth > 0) ? window.innerWidth : screen.width;
	},
	getScreenHeight: function(){
		return (window.innerHeight > 0) ? window.innerHeight : screen.height;
	},
	getDeviceType: function(){
		if (typeof(device) != 'undefined' && typeof(device.name) != 'undefined'){
			return device.name;
		}
		return Ext.os.deviceType;
	},
	getDeviceUUID: function(){
		if (typeof(device) != 'undefined' && typeof(device.uuid) != 'undefined'){
			return device.uuid;
		}
		//return "00000000-0000-0000-0000-000000000000";
		return "0";
	},
	isDesktop: function(){
		return Ext.os.deviceType === 'Desktop';
	},
	isPhone: function(){
		return Ext.os.deviceType === 'Phone';
	},
	isTablet: function(){
		return Ext.os.deviceType === 'Tablet';
	},
	isDevice: function(){
		return this.isPhone() || this.isTablet();
	},
	isBigScreen: function(){
		//if (this.isDesktop() || this.isTablet()) return true;
		if (Math.max(Global().getScreenWidth(), Global().getScreenHeight()) >= 1024) return true;
		return false;
	},
	bigSmall: function(big, small){
		if (Global().isBigScreen()) return big;
		return small;
	},
	updateBackButton: function(id){
		if (Global().isBigScreen() && Ext.getCmp(id)){
			Ext.getCmp(id).setWidth('120px');
			Ext.getCmp(id).setHeight('50px');
		}
	},
	getTopPlaceholder: function(){
        return {
            xtype: 'toolbar',
            docked: 'top',
            id: 'dummyToolbar',
            minHeight: '21px',
            items: []
        };
	},
	getSyncButton: function(id){
		//if (Global().isBigScreen() == false && id != "sync_consoleSurgeon") return {hidden: true};
		
		return {
        	id: id,
        	itemId: id,
        	width: 45,
        	height: 40,
        	docked: 'right',
        	html: '<img style="-webkit-filter: grayscale(1);margin-top:4px;" src="appresource/icons/sync_22_22.png">',
        	action: 'onSyncButtonTap'
        	/*listeners: [{
    			event: 'tap',
    			fn: Global().sync.onClick
    		}]*/
        };
	},
	dropAllTables: function(){
		Main().executeSql("select * from sqlite_master where type='table'", function(tx, r){
			Global().log(r.rows.length);
			for (var i=0;i<r.rows.length;i++){
				var table = r.rows.item(i).name;
				if (table.charAt(0) == '_') continue;
				Global().log(table);
				Main().executeSql("drop table "+table);
			}
		});
	},
	downloadAllTables: function(){
		var tot = 0;
		var totalTable = 0;
		var sqls = [];
		Main().executeSql("select * from sqlite_master where type='table'", function(tx, r){
			totalTable = r.rows.length;
			Global().log(totalTable);
			for (var i=0;i<r.rows.length;i++){
				var table = r.rows.item(i).name;
				if (table.charAt(0) == '_') {
					totalTable--;
					continue;
				}
				Global().log(table);
				Main().executeSql("select * from "+table, function(transaction, results){
					tot++;
					
					if (results.rows.length > 0){
						// create table sql
						var fields = [];
						for (var field in results.rows.item(0)){
							fields.push(field);
						}
						sqls.push("create table "+table+" ("+fields.join(",")+");");
						
						// insert data
						for (var i=0;i<results.rows.length;i++){
							var values = [];
							for (var j=0;j<fields.length;j++){
								values.push(results.rows.item(i)[fields[j]]);
							}
							sqls.push("insert into "+table+" values ('"+values.join("','")+"');");
						}
						
						// check finish
						if (tot === totalTable){
							Global().download("database.txt", sqls.join("\n"));
						}
					}
				});
			}
		});
	},
	/* sync is needed to manage offline data management. */
	sync: {
		onClick: function(){
			//Global().log("Inside Global().sync.onClick()");
			//View().show("SubmittedList", {direction: 'left'});
			Global().alert("Notification", "This will sync the browser data to the server. Currently not implemented. Do you want to download the browser sql?", function(button){
				button = button.toUpperCase();
				if (button == "YES"){
					Global().downloadAllTables();
				}
			}, ["Yes", "No"]);
		}
	},
	alert: function(title, msg, callback, buttons){
    	if (typeof(callback) == 'undefined'){
    		callback = function(){};
    	}
    	if (typeof(buttons) === 'undefined'){
    		buttons = ["OK"];
    	}
    	if (typeof(msg) == 'undefined' || (typeof(msg) == 'string' && msg.length == 0)){
    		msg = ' ';
    	}
		Ext.device.Notification.show({
			title: title,
			message: msg,
			buttons: buttons,
			callback: callback
		});    	
	},
	
	log: function(obj){
		try{
			if (Global().getDeviceType() === 'Desktop'){
				console.log(obj);
			} else {
				console.log = Global().log;
				//if (typeof(obj) == 'string' && obj.indexOf("**") == 0){} else {return;}
				Cordova.exec(function(result) {
				}, function(error) {}, 
				"consolelog", "log", [ JSON.stringify(obj) ]);
			}
		}catch(error){}
	},
	download: function(filename, text) {
	    var pom = document.createElement('a');
	    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	    pom.setAttribute('download', filename);
	    pom.click();
	}	
});
var Global = function(){return shipwire.util.Global;};
