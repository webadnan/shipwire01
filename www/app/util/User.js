/**
 * SM Adnan, added on Jan 31 2014
 * Target of this class
 * Execute all User related task through this class
 */
Ext.define('shipwire.util.User', {
	singleton: true,
	requires: [
	       	],
	isReady: false,
	table: 'TT_USER_DATA',
	password: '',
	fields: [
		'userID',
		'userName',
		'password',
		'emailAddress',
		'employeeID',
		'loginDate',
		'dataSyncTime',
		'initialDataVersion',
		'appVersion',
		'appUpdateUrl',
		'consoleSurgeonsDeltaTime',
		'consoleSurgeonsTimestamp', // this field direcly comes from json
		],
		
	values: [], // value of current user data
	
	removeAll: function(){
		for (var index in User().fields){
			var field = User().fields[index];
			if (['password','dataSyncTime', 'initialDataVersion', 'consoleSurgeonsDeltaTime', 'consoleSurgeonsTimestamp'].indexOf(field) !== -1) {
				continue;
			}
			User().set(field, null);
		}
	},
	get: function(field){
		if (!User().isReady) throw Error ('User class is not ready');
		var index = this.fields.indexOf(field);
		if (index == -1) throw Error(field+' is not available in '+this.table);
		
//		// TODO remove this code
//		if (field === 'password' && User().get('userID') === 'adnans' && Global().getDeviceType() === 'Desktop'){
//			return 'Intsurg2';
//		}
		
		var value = this.values[index];
		// Field specific logic
		if (field === 'password'){
			return User().password;
		} else if (field === 'dataSyncTime' || field === 'consoleSurgeonsDeltaTime'){
			if (value === null){
				return new Date(1005092526075);
			} else {
				return new Date(value);
			}
		} else if (field === 'initialDataVersion'){
			if (value === null){
				return "00000000000000";
			}
		}
		return value;
	},
	set: function(field, value){
		if (!User().isReady) {
			throw Error ('User class is not ready');
		}
		var index = this.fields.indexOf(field);
		if (index == -1) {
			throw Error(field+' is not available in '+this.table);
		}
		// Field specific logic
		if (value !== null && typeof(value) === 'object'){
			value = value.toString();
		}
		if (field == 'password'){
			User().password = value;
			return;
			//value = Global().encrypt(value);
		}
		
		if (value !== null) {
			Main().executeSql("update "+this.table+" set "+field+" = '"+value+"'");
		} else {
			Main().executeSql("update "+this.table+" set "+field+" = null");
		}
		this.values[index] = value;
	},
	load: function(callback){
		this.createTable(function(){
			var sql = 'select '+Global().convertArrayToCSV(User().fields)+' from '+User().table;
			Main().executeSql(sql, function(tx, results){
				var row = results.rows.item(0);
				for (var index in User().fields){
					var field = User().fields[index];
					User().values.push(row[field]);
				}
				User().isReady = true;
				callback();
			});
		});
	},
	createTable: function(callback){
		var sqlCreate = 'CREATE TABLE IF NOT EXISTS '+User().table+' ('+Global().convertArrayToCSV(User().fields)+')';
		var sqlCheckData = 'select count(*) as cnt from '+User().table;
		var sqlInsertData = "insert into "+User().table+" (userID) values (null)";
		Main().executeSql(sqlCreate, function(tx, r){
			tx.executeSql(sqlCheckData, [], function(tx, results){
				if (parseInt(results.rows.item(0).cnt) === 0){ // no data found
					if (callback) tx.executeSql(sqlInsertData, [], callback());
					else tx.executeSql(sqlInsertData);
				} else if (callback) {
					callback();
				}
			});
		});
	},
	/**
	 * Returns milliseconds since last synced
	 */
	getDataSyncDuration: function(){
		return new Date().getTime() - this.get('dataSyncTime').getTime();
	},
	/**
	 * Returns milliseconds since last synced
	 */
	getConsoleSurgeonsDeltaDuration: function(){
		return new Date().getTime() - this.get('consoleSurgeonsDeltaTime').getTime();
	},
	getAuthKey: function(){
		//["TRAINTRACK {USERNAME}:{PASSWORD}", "TRAINTRACK", " ", ":"]
		var tokens = GenericMap().AUTHORIZATION_VALUE.match(/([^{ ]*)(.*){USERNAME}(.*){PASSWORD}/);
		var prefix = tokens[1];
		var space = tokens[2];
		var separator = tokens[3];
		var encoded = User().get('userID') + separator + User().get('password');
		encoded = Global().base64encode(encoded);
		var key = prefix + space + encoded;
		return key;
	},
	getAuthHeaders: function(){
		var headers = {};
		headers[GenericMap().AUTHORIZATION_KEY] = User().getAuthKey();
		Global().log(GenericMap().AUTHORIZATION_KEY);
		Global().log(User().getAuthKey());
		return headers;
	},
	hasUserIDAndPassword: function(){
		/*if (Global().isDesktop()){
			return true;
		}*/
		return !(User().get('userID') === null || User().get('password') === null || User().get('userID') === '' || User().get('password') === '');
	}
});
var User = function(){return shipwire.util.User;};
