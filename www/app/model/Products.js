/**
 * SM Adnan, added on Mar 23 2014
 */
Ext.define('shipwire.model.Products', {
	singleton: true,
	requires: [
	],
	
	keys: {},
	
	table: 'Products',
	fields: "id|Name|Description|Width|Length|Height|Weight|Value",
	
	data: [],
	
	load: function(callback){
		var me = Products();
		Main().isTableExists(me.table, function(flag){
			if (!flag){
				console.log('Product table not found');
				Main().executeSql("create table "+me.table+" ("+me.fields.split("|").join(",")+")", function(){
					insertData();
					callback();
				});
			}else{
				Main().executeSql("select * from Products", function(tx, results){
					if (results.rows.length == 0){
						insertData();
					}
					for (var i=0;i<results.rows.length;i++){
						
					}
				});
			}
		});
		
		function insertData(){
			var sqls = [];
			//sqls.push("create table Products (id,Name,Description,Width,Length,Height,Weight,Value);");
			sqls.push("insert into Products values ('076929c5-93e3-46fb-bd1c-cff15a64308e','Headphone','Headphone with integrated Microphone','30cm','50cm','20cm','1.23lbs','5.75')");
			sqls.push("insert into Products values ('05977c7b-3ab1-476a-83d9-15ce6846c68c','HP Laptop','HP notebooks are thoughtfully crafted with leading-edge technologies, subtle but powerful details, style and design. We make technology that works for you.','51cm','60cm','5cm','5lbs','450')");
			sqls.push("insert into Products values ('0fa5aab6-427b-419d-897c-dbb9fa512a26','Enterprise Solution Pack','SafeWord 2008 Management Console provides enterprise wide management of users, tokens and access rights as well as event logging and reporting.','10cm','10cm','10cm','5lbs','500')");
			sqls.push("insert into Products values ('1dbcdf77-2682-4863-84af-61025be17baa','Sentinel HASP Business Studio','Sentinel HASP Business Studio is a tool which your product and marketing staff use to prepare your software product for market. It includes all of the tools necessary to appropriately license and lock your application to a Sentinel HASP HL hardware key, or a Sentinel HASP SL software product key after protection is implemented.','NA','NA','NA','NA','1000')");
			sqls.push("insert into Products values ('3ee9ccc5-ac5a-4cbe-8907-9342d5afce96','SafeNet Authentication','SafeNet, Inc., a global leader in information security, announced today that its eToken PRO, eToken PRO Anywhere, and eToken NG-FLASH Anywhere certificate-based authentication token devices, as well as its Luna and ProtectServer.','NA','NA','NA','NA','700')");
			sqls.push("insert into Products values ('8ceb6221-d24f-4bdc-894c-d97c917f86ae','ASUS MeMO Pad FHD 10','Asus tablet with OS Android was awarded for it would be difficult to find a better brand tablet with 32 GB of memory, 3G modem and display such high resolutions for such good price, accordingly to the editors.','20cm','25cm','3cm','1.2lbs','400')");
			sqls.push("insert into Products values ('aa0cc137-927b-468e-8c94-00ae59b3b66f','ASUS Chromebox','With easy out-of-the-box setup, integrated virus and malware protection and feature-enhancing updates, ASUS Chromebox starts up in seconds to get you to your favorite websites and apps instantly.','12cm','20m','8cm','4lbs','250')");
			sqls.push("insert into Products values ('0ef2a48f-5514-4d9b-83d9-8871a1662d72','ASUS Republic of Gamers','ASUS Republic of Gamers (ROG) today announced the G750JZ, G750JM and G750JS, new additions to its highly acclaimed range of G-Series high-performance gamer-centric notebooks.','30cm','40cm','5cm','7lbs','1200')");
			sqls.push("insert into Products values ('70eb8832-f07a-4de7-8711-adddaad3cdd6','Test','Alibaba','NA','NA','NA','NA','10');");
			sqls.forEach(function(s){
				Main().executeSql(s);
			});
		}
	},
	addProduct: function(product, callback){
		var me = Products();
		product['id'] = Global().createUUID();
		var values = [];
		me.fields.split('|').forEach(function(field){
			values.push(product[field]);
		});
		var sql = "insert into "+me.table+" ("+me.fields.split("|").join(",")+") values ('" + values.join("','") + "')";
		Global().log(sql);
		Main().executeSql(sql, callback);
	},
	updateProduct: function(product){
		var me = Products();
		var sqls = [];
		me.getDisplayFields().forEach(function(field){
			var sql = field+"='"+product[field]+"'";
			sqls.push(sql);
		});
		var esql = "update "+me.table+" set "+sqls.join(",")+" where id='"+product.id+"'";
		Main().executeSql(esql);
	},
	getProducts: function(callback){
		var me = Products();
		Main().executeSql("select * from "+me.table, function(tx, r){
			if (r.rows.length == 0){
				callback([]);
				return;
			}
			var rows = [];
			for (var i=0;i<r.rows.length;i++){
				rows.push(r.rows.item(i));
			}
			callback(rows);
		}, function(){
			callback([]);
		});
	},
	getProduct: function(id, callback){
		var me = Products();
		Main().executeSql("select * from "+me.table+" where id='"+id+"'", function(tx, r){
			if (r.rows.length == 0){
				callback(null);
				return;
			}
			callback(r.rows.item(0));
		}, function(){
			// error
			callback(null);
		});
	},
	getDBFields: function(){
		var me = Products();
		return me.fields.split("|");
	},
	getDisplayFields: function(){
		var me = Products();
		return me.fields.split("|").slice(1);
	},
	deleteProduct: function(id){
		var me = Products();
		Main().executeSql("delete from "+me.table+" where id='"+id+"'");
	}
});
var Products = function(){return shipwire.model.Products;};
