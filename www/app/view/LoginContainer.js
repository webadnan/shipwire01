/*
 * File: app/view/LoginContainer.js
 */
Ext.define('shipwire.view.LoginContainer', {
    extend: 'Ext.Container',
    alias: 'widget.LoginContainer',

    config: {
        id: 'LoginContainer',
        itemId: 'LoginContainer',
        layout: {
            type: 'fit'
        },
        listeners: [{
        	event: 'activate',
        	fn: function(){
        		Global().log("Login activated");
        	}
        }],
        items: [
            Global().getTopPlaceholder(),
            {
                xtype: 'titlebar',
                docked: 'top',
                title: 'Shipwire Project'
            },
            {
                xtype: 'formpanel',
                id: 'LoginForm',
                itemId: 'myformpanel',
                items: [
                    {
                        xtype: 'fieldset',
                        items: [
                            {
                                xtype: 'textfield',
                                id: 'userName',
                                label: '',
                                autoComplete: false,
                                autoCorrect: false,
                                placeHolder: 'User Name'
                            },
                            {
                                xtype: 'passwordfield',
                                id: 'password',
                                itemId: 'password',
                                label: '',
                                placeHolder: 'Password'
                            },
                            {
                                xtype: 'button',
                                id: 'LoginButton',
                                itemId: 'LoginButton',
                                ui: 'action-round',
                                text: 'Login',
                                listeners: [{
                                	event: 'tap',
                                	fn: function(){
                                		setTimeout(Main().onLoginButtonTap, 100);
                                	}
                                }]
                            },
                            {
                                xtype: 'label',
                                height: 39,
                                id: 'placeholder1',
                                ui: 'light',
                                html: 'this will accept any username/password',
                                style: {
                                	marginTop: '10px',
                                	marginLeft: '10px'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        height: 39,
                        id: 'loginErrMsg',
                        ui: 'light',
                        width: 284
                    },
                ]
            }
        ]
    }
});
