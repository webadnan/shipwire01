/*
 * SM Adnan, 23 MAR 2014
 */

Ext.define('shipwire.view.UserInfo', {
	extend: 'Ext.Container',
	alias: 'widget.UserInfo',
	config: {
		id: 'UserInfo',
		itemId: 'UserInfo',
		layout: {
			type: 'fit'
		},
		listeners: [{
				fn: 'onActivated',
				event: 'activate'
			}, {
				fn: 'onDeactivated',
				event: 'deactivate'
			}],
		items: [
			Global().getTopPlaceholder(),
			{
				xtype: 'toolbar',
				docked: 'top',
				id: 'UserInfoToolbar',
				title: '<div style="font-size:.8em;">User Info</div>',
				items: [{
						xtype: 'button',
						id: 'UserInfoBack',
						itemId: 'UserInfoBack',
						ui: 'back',
						text: 'Back',
						listeners: [{
							event: 'tap',
							fn: View().showPrevious
						}]
					}
					]
			},
			{
				xtype: 'formpanel',
				id: 'UserInfoContainer',
				//height: '240px',
				scrollable: {
					direction: 'horizontal',
					directionLock: true,
					indicators: false
				}
			}
		]
	},
	onActivated: function() {
		Global().log('UserInfo Activated');
		var me = this;
		
        Global().updateBackButton('UserInfoBack');
	},
	onItemTap: function(target, button){
    	if (IgnoreTapWithin().check("UserInfo.onItemTap", 500)){
    		return;
    	}
    	
		var id = button.id;
		console.log(id);
	},
	onDeactivated: function() {
		Global().log('UserInfo deactivated');
	}
});
