/*
 * SM Adnan, 23 MAR 2014
 */
Ext.define('shipwire.view.ProductCatalog', {
	extend: 'Ext.Container',
	alias: 'widget.ProductCatalog',
	index2id: {},
	config: {
		id: 'ProductCatalog',
		itemId: 'ProductCatalog',
		layout: {
			type: 'fit'
		},
		listeners: [{
				fn: 'onActivated',
				event: 'activate'
			}, {
				fn: 'onDeactivated',
				event: 'deactivate'
			}],
		items: [
			Global().getTopPlaceholder(),
			{
				xtype: 'toolbar',
				docked: 'top',
				id: 'ProductCatalogToolbar',
				title: '<div style="font-size:.8em;">Product Catalog</div>',
				items: [{
						xtype: 'button',
						id: 'ProductCatalogBack',
						itemId: 'ProductCatalogBack',
						ui: 'back',
						text: 'Back',
						listeners: [{
							event: 'tap',
							fn: View().showPrevious
						}]
					}
					]
			},
			{
				xtype: 'formpanel',
				id: 'ProductCatalogContainer',
			}
		]
	},
	onActivated: function() {
		Global().log('ProductCatalog Activated');
		var me = this;
        Global().updateBackButton('ProductCatalogBack');
        me.createUI();
	},
	createUI: function(){
		var me = this;
		
        me.getContainer().removeAll();
        var htmls = [];
        Products().getProducts(function(rows){
        	rows.forEach(function(row, index){
        		htmls.push(me.createCell(row, index, rows.length));
        		me.index2id[index] = row.id;
        	});
        	
        	htmls.splice(0, 0, '<div style="margin: 20px 10px 10px 10px;">');
        	htmls.push("</div>");
        	
            me.getContainer().add({
            	xtype: 'label',
            	html: htmls.join("\n")
            });
            me.registerEvents();
        });
	},
	registerEvents: function(){
		var me = this;
		$("[id^=cell_]").each(function(i, e){
			Ext.get(e.id).on('tap', me.onItemTap);
		});
	},
	createCell: function(row, index, tot){
		var me = this;
		var cls = '';
		if (index ==0){
			cls = ' tt-cell-top ';
		} else if (index+1 == tot){
			cls = ' tt-cell-bottom ';
		}
		var html = '';
		html += '<div id="cell_'+index+'" class="tt-cell '+cls+'">';
		html += '   <font style="pointer-events: none;" class="tt-skill-text">'+row.Name+'</font>';
		html += '   <div name="command_'+index+'" style="float:right;font-size:.7em;display:none;">';
		html += '      <a id="command_edit_'+index+'" href="#" click="Ext.getCmp(\'ProductCatalog\').onEditClick();" class="product_catalog_a">edit</a>';
		html += '      | ';
		html += '      <a id="command_delete_'+index+'" href="#" click="Ext.getCmp(\'ProductCatalog\').onDeleteClick();" class="product_catalog_a">delete</a>';
		html += '   </div>';
		html += '   <div style="font-size:.7em;pointer-events: none;">'+row.Description+'</div>';
		html += '   <div name="detail_'+index+'" style="display: none;font-size: .7em; font-style: italic; margin: 10px 0 0 10px;pointer-events: none;">'+me.cellDetail(row)+'</div>';
		html += '</div>';
		return html;
	},
	cellDetail: function(row){
		var h = '<table>';
		"Width|Length|Height|Weight|Value".split("|").forEach(function(field){
			h += "<tr><td>"+field+":</td><td>&nbsp; &nbsp; </td><td>"+row[field]+"</td></tr>";
		});
		h += '</table>';
		return h;
	},
	getContainer: function(){
		return Ext.getCmp("ProductCatalogContainer");
	},
	onItemTap: function(target, e){
		var me = Ext.getCmp("ProductCatalog");
    	if (IgnoreTapWithin().check("ProductCatalog.onItemTap", 50)){
    		return;
    	}
		console.log(id);
    	
    	if (e.id.indexOf("command_delete") == 0){
    		var index = e.id.split("_")[2];
    		Global().alert("Confirmation", "Do you want to delete this product.", function(button){
    			if (button.toUpperCase() === "YES"){
    				Global().log("Delete this record");
    				Global().log(me.index2id);
    				var productID = me.index2id[index];
    				Global().log("Deleting product with id: "+productID);
    				Products().deleteProduct(productID);
    				$("#cell_"+index).remove();
    				delete me.index2id[index];
    			}
    		}, ["Yes", "No"]);
    	} else if (e.id.indexOf("command_edit") == 0){
    		var index = e.id.split("_")[2];
    		var productID = me.index2id[index];
    		Products().getProduct(productID, function(product){
    			Global().currentProduct = product;
        		View().show("AddProduct", {direction: 'left'});
    		});
    	} else {
    		var id = e.id;
    		var index = id.split("_")[1];
    		$("[name^=detail_]").hide();
    		$("[name=detail_"+index+"]").show();
    		$("[name^=command_]").hide();
    		$("[name=command_"+index+"]").show();
    	}
	},
	onDeactivated: function() {
		Global().log('ProductCatalog deactivated');
	}
});
