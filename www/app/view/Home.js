/*
 * SM Adnan, 23 MAR 2014
 */
Ext.define('shipwire.view.Home', {
	extend: 'Ext.Container',
	alias: 'widget.Home',
	config: {
		id: 'Home',
		itemId: 'Home',
		layout: {
			type: 'fit'
		},
		listeners: [{
				fn: 'onActivated',
				event: 'activate'
			}, {
				fn: 'onDeactivated',
				event: 'deactivate'
			}],
		items: [
			Global().getTopPlaceholder(),
			{
				xtype: 'toolbar',
				docked: 'top',
				id: 'HomeToolbar',
				title: '<div style="font-size:.8em;">Home</div>',
				items: [{
						xtype: 'button',
						id: 'HomeBack',
						itemId: 'HomeBack',
						ui: 'back',
						text: 'Logout',
						listeners: [{
							event: 'tap',
							fn: View().showPrevious
						}]
					},
					Global().getSyncButton('sync_Home')
					]
			},
			{
				xtype: 'formpanel',
				id: 'HomeContainer',
				//height: '240px',
				scrollable: {
					direction: 'horizontal',
					directionLock: true,
					indicators: false
				}
			}
		]
	},
	onActivated: function() {
		Global().log('Home Activated');
		var me = this;
		
        Global().updateBackButton('HomeBack');
       
		var html = '';
		html += '<div id="HomeScrollPanel" style="width: 2000px; text-align: center; margin: 0 auto; margin-top: 20px;">';
        html += me.createCell('home_userinfo', 'User Info');
        html += me.createCell('home_newproduct', 'Add New Product');
        html += me.createCell('home_productcatalog', 'Product Catalog');
		html += '</div>';
		Ext.getCmp("HomeContainer").add({
			xtype: 'label',
			html: html
		});
		
		var totWidth = 0;
		$("#HomeScrollPanel > div").each(function(i, e){
			totWidth += $(e).width() + parseInt($(e).css('margin-left')) + parseInt($(e).css('margin-right'));
		});
		$("#HomeScrollPanel").width(totWidth+30);
		
		// Registering event
		$("#HomeScrollPanel > div").each(function(i, e){
			console.log(e.id);
			Ext.get(e.id).on('tap', me.onItemTap);
		});
	},
	createCell: function(id, title){
		var h = '';
		h += '<div id="'+id+'" class="main-cell-single" style="float:left;">';
		h += '<br><br><br>';
		h += title;
		h += '</div>';
		return h;
	},
	onItemTap: function(target, button){
    	if (IgnoreTapWithin().check("Home.onItemTap", 500)){
    		return;
    	}
    	
		var id = button.id;
		console.log(id);
		var index = "home_userinfo|home_newproduct|home_productcatalog".split("|").indexOf(id);
		var viewName = "UserInfo|AddProduct|ProductCatalog".split("|")[index];
		View().show(viewName, {direction: 'left'});
		
	},
	onDeactivated: function() {
		Global().log('Home deactivated');
	}
});
