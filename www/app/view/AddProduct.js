/*
 * SM Adnan, 23 MAR 2014
 */
Ext.define('shipwire.view.AddProduct', {
	extend: 'Ext.Container',
	alias: 'widget.AddProduct',
	config: {
		id: 'AddProduct',
		itemId: 'AddProduct',
		layout: {
			type: 'fit'
		},
		listeners: [{
				fn: 'onActivated',
				event: 'activate'
			}, {
				fn: 'onDeactivated',
				event: 'deactivate'
			}],
		items: [
			Global().getTopPlaceholder(),
			{
				xtype: 'toolbar',
				docked: 'top',
				id: 'AddProductToolbar',
				title: '<div style="font-size:.8em;">Add Product</div>',
				items: [{
						xtype: 'button',
						id: 'AddProductBack',
						itemId: 'AddProductBack',
						ui: 'back',
						text: 'Back',
						listeners: [{
							event: 'tap',
							fn: function(){
								Ext.getCmp("AddProduct").onBackButtonTap();
							}
						}]
					}
					]
			},
			{ 
				xtype: 'formpanel',
				id: 'AddProductContainer',
			}
		]
	},
	onActivated: function() {
		Global().log('AddProduct Activated');
		var me = Ext.getCmp("AddProduct");
		
        Global().updateBackButton('AddProductBack');
        
        Products().getDisplayFields().forEach(function(field, index){
        	me.container().add({
        		xtype: 'textfield',
        		id: 'product_'+field,
        		placeHolder: field,
        		value: me.getDefaultValue(field),
        		style: {
        			marginLeft: '10px',
        			marginRight: '10px',
        			marginTop: (index === 0 ? '10px' : '0px'),
        			marginBottom: '1px'
        		}
        	});
        });
        me.container().add({
            xtype: 'button',
            id: 'AddProductButton',
            itemId: 'AddProductButton',
            text: me.getButtonText(),
            style: {
            	margin: '0 auto',
            	margin: '10px 10px 10px 10px'
            },
            ui: 'confirm',
            height: 60,
            width: '30%',
            listeners: [{
            	event: 'tap',
            	fn: function(){
            		setTimeout(me.onAddProductButtonTap, 100);
            	}
            }]
        });
	},
	getDefaultValue: function(field){
		if (Global().currentProduct == null){
			return '';
		} 
		return Global().currentProduct[field];
	},
	getButtonText: function(){
		if (Global().currentProduct == null){
			return 'Add';
		} 
		return 'Edit';
	},
	getProduct: function(){
		var me = Ext.getCmp("AddProduct");
		var product = {};
		Products().getDisplayFields().forEach(function(field){
			var value = me.container().getComponent("product_"+field).getValue().trim();
			product[field] = value;
		});
		return product;
	},
	isValidInput: function(){
		var me = Ext.getCmp("AddProduct");
		var product = me.getProduct();
		if (product.Name == "" || product.Value == ""){
			Global().alert("Validity", "Name and Value cannot be empty");
			return false;
		}
		if (isNaN(product.Value)){
			Global().alert("Validity", "Please enter a correct value");
			return false;
		}
		return true;
	},
	container: function(){
		return Ext.getCmp("AddProductContainer");
	},
	onItemTap: function(target, button){
    	if (IgnoreTapWithin().check("AddProduct.onItemTap", 500)){
    		return;
    	}
    	
		var id = button.id;
		console.log(id);
	},
	onAddProductButtonTap: function(){
		var me = Ext.getCmp("AddProduct");
		var buttonText = Ext.getCmp("AddProductContainer").getComponent("AddProductButton").getText().toUpperCase();
		var product = me.getProduct();
		if (buttonText === "EDIT"){
			product.id = Global().currentProduct.id;
			Global().log("Editing the following product");
			Global().log(product);
			Products().updateProduct(product);
			View().showPrevious();
		} else {
			if (me.isValidInput()){
				Global().alert("Success", "Data has been added to the database. You can check the product in Product Catalog page.", function(){
					Products().addProduct(product);
					View().showPrevious();
				});
			}
		}
	},
	onBackButtonTap: function(){
		var me = Ext.getCmp("AddProduct");
		var product = me.getProduct();
		if (product.Name != ""){
			Global().alert("Notification", "Do you want to go back?", function(button){
				if (button.toUpperCase() === "OK"){
					View().showPrevious();
				}
			}, ["OK", "Cancel"]);
		} else {
			View().showPrevious();
		}
	},
	onDeactivated: function() {
		Global().log('AddProduct deactivated');
	}
});
