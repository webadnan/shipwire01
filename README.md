/*
Name: SM Adnan
Email: webadnan@gmail.com
Shipwire Test Project
*/

Starting point
------------------------------------------------------------------------------------------
You can test the project by the opening the index.html file from www folder.
Or you can open the link from http://thelogicforum.com/shipwire

Overview
------------------------------------------------------------------------------------------
This project is develop using HTML5, CSS3, JavaScript
with JavaScript Framework like Sencha Touch (ExtJS), jQuery, PhoneGap.

This app stores the database locally in SQLite database supported by HTML5 - webkit
supported browsers, like Chrome, Safari. Database can be downloaded through sync button.

Usages
------------------------------------------------------------------------------------------
It accepts any username/password and even blank. After login, it shows the home page
of User, Add Products and Product Catalog. Currently User page does not contains any
information. Add product page is used to add new product, a little validation check is
there. In ProductCatalog page it shows all the products available in database. By
clicking each cell you can see the detail info of that product and also Edit and Delete
link will be visible. Those are functional.

Advantages
------------------------------------------------------------------------------------------
- This app is developed using PhoneGap and thus it can be deployed to iOS and Android without
modifying a single line of code. iOS version is currently available. Please let me know
if you need that version: webadnan@gmail.com

- The project is developed using MVC pattern. Tried to use pure JavaScript with ExtJS library
to show visual effects and jQuery for DOM parsing.

- After modifying the data there is a Sync button at top right corner of Home page. That will
sync the modification with the server. In this way this application can have capability
to work offline without any trouble. I have implemented the similar mobile application 
in current company.

- For testing I have added a feature to create database dump file and make it downloaded
without the need of a server. By clicking the sync button you can able to do that.

Disadvantages
------------------------------------------------------------------------------------------
- As it is developed with webkit enabled browser it supports only Chrome, Safari, Android,
iOS, Blackberry. But not IE and Firefox.
